from setuptools import setup
from setuptools import find_packages
import os
import sys

py3k = sys.version_info > (3, )

readme = os.path.join(os.path.dirname(__file__), 'README.rst')

requires = [
    'SQLAlchemy',
    'psycopg2',
    'psycogreen',
    'gevent',
    'pymysql',
    'mysqlclient'
]

if py3k:
    requires.append('aiopg')

setup(
    name='throughput_suite',
    version='1.0',
    description="Send / receive a lot of data to / from a "
                "database and measure throughput",
    long_description=open(readme).read(),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Topic :: Database :: Front-Ends',
    ],
    keywords='SQLAlchemy HA',
    author='Mike Bayer',
    author_email='mike@zzzcomputing.com',
    url='http://bitbucket.org/zzzeek/throughput_suite',
    license='MIT',
    packages=find_packages('.', exclude=['examples*', 'test*']),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    entry_points={
        'console_scripts': ['throughput-suite = throughput_suite.cmd:main'],
    }
)
