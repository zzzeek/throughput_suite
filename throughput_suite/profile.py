import time
import threading
import multiprocessing


class avg_rec_rate(object):
    def __init__(self):
        self.mutex = threading.Lock()

    _run = False

    def _init_counters(self):
        self.count = 0
        self.tps = 0

    def start(self, name):
        if self._run:
            self.stop()

        print("Profiling section: %s" % name)
        self._init_counters()
        self._run = True
        self.worker = threading.Thread(target=self._maintain)
        self.worker.start()

    def stop(self):
        self._run = False
        self.worker.join()

    def _maintain(self):
        last_time = time.time()
        last_count = 0
        last_tps = 0

        metrics = []
        tps_metrics = []

        while self._run:
            time.sleep(5)

            count = self.count
            tps = self.tps

            # don't start measuring too soon.
            if count < 10000:
                continue

            now = time.time()
            time_delta = now - last_time
            end_timestamp = now

            last_time = now

            # TODO: abstract out the concept of a "metric"
            # into some kind of object

            count_delta = count - last_count
            rate = count_delta / time_delta
            last_count = count

            if rate:
                metrics.append(rate)

            avg = sum(metrics) / len(metrics)

            tps_delta = tps - last_tps
            tps_rate = tps_delta / time_delta
            last_tps = tps

            if tps_rate:
                tps_metrics.append(tps_rate)

            tps_avg = sum(tps_metrics) / len(tps_metrics)

            print(
                "%s Total count: %d, %.2f recs/sec "
                "%.2f t/sec "
                "%.2f avg recs/sec %.2f avg t/sec" %
                (
                    time.strftime(
                        "%Y-%m-%d %H:%M:%S",
                        time.localtime(end_timestamp)),
                    count,
                    metrics[-1],
                    tps_metrics[-1],
                    avg, tps_avg
                )
            )

    def tag(self, count=1):
        with self.mutex:
            self.count += count

    def tag_tps(self):
        with self.mutex:
            self.tps += 1


class mproc_avg_rec_rate(avg_rec_rate):
    def __init__(self):
        self._count = multiprocessing.Value('d', 0.0)
        self._tps = multiprocessing.Value('d', 0.0)

    def _init_counters(self):
        with self._count.get_lock():
            self._count.value = 0
        with self._tps.get_lock():
            self._tps.value = 0

    @property
    def count(self):
        return self._count.value

    @property
    def tps(self):
        return self._tps.value

    def tag(self, count=1):
        with self._count.get_lock():
            self._count.value += count

    def tag_tps(self):
        with self._tps.get_lock():
            self._tps.value += 1
