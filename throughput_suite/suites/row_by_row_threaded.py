from ..profile import avg_rec_rate
from .. import util
from ..compat import queue
from sqlalchemy.engine import url
import threading

connect = None
work_queue = None
monitor = None
dbapi = None


def setup(options):
    global dbapi
    db_url = url.make_url(options.dburl)

    dialect_cls = db_url.get_dialect()
    dbapi = dialect_cls.dbapi()
    dialect = dialect_cls(dbapi=dbapi)
    cargs, cparams = dialect.create_connect_args(db_url)

    global connect

    def connect():
        conn = dbapi.connect(*cargs, **cparams)

        # async runs like this, so shall we!
        # if comparing to async with psycopg2,
        # use the autocommit option
        if options.autocommit:
            conn.autocommit = True
        return conn


def _get_connection():
    return connect()


def worker(options):
    use_transaction = not options.autocommit
    allow_executemany = options.allow_executemany
    num_recs = 0

    conn = _get_connection()
    cursor = conn.cursor()
    while True:
        item = work_queue.get()

        try:
            num_recs += 1

            # "row by row" means, we aren't being smart at all about
            # chunking, executemany(), or looking up groups of dependent
            # records in advance.

            if item['type'] == "geo":
                cursor.execute(
                    "insert into geo_record (fileid, stusab, chariter, "
                    "cifsn, logrecno) values (%s, %s, %s, %s, %s)",
                    (item['fileid'], item['stusab'], item['chariter'],
                     item['cifsn'], item['logrecno'])
                )
                monitor.tag(1)
            else:
                cursor.execute(
                    "select id from geo_record where fileid=%s and logrecno=%s",
                    (item['fileid'], item['logrecno'])
                )
                row = cursor.fetchone()
                geo_record_id = row[0]

                cursor.execute(
                    "select d.id, d.index from dictionary_item as d "
                    "join matrix as m on d.matrix_id=m.id where m.segment_id=%s "
                    "order by m.sortkey, d.index",
                    (item['cifsn'],)
                )
                dictionary_ids = [
                    _row[0] for _row in cursor
                ]
                if len(dictionary_ids) != len(item['items']):
                    print(
                        "dictionary ids for cifsn %s length %d "
                        "does not match length of items %d" % (
                            item['cifsn'],
                            len(dictionary_ids),
                            len(item['items'])
                        )
                    )

                if allow_executemany:
                    cursor.executemany(
                        "insert into data_element "
                        "(geo_record_id, dictionary_item_id, value) "
                        "values (%s, %s, %s)",
                        [
                            (geo_record_id, dictionary_id, element)
                            for dictionary_id, element in
                            zip(dictionary_ids, item['items'])
                        ]
                    )
                else:
                    for dictionary_id, element in zip(dictionary_ids, item['items']):
                        cursor.execute(
                            "insert into data_element "
                            "(geo_record_id, dictionary_item_id, value) "
                            "values (%s, %s, %s)",
                            (geo_record_id, dictionary_id, element)
                        )
                monitor.tag(len(item['items']))

            if use_transaction:
                monitor.tag_tps()
                conn.commit()
        except dbapi.Error as err:
            print("DBAPI error encountered: %s, re-queuing record" % err)
            if use_transaction:
                conn.rollback()
            work_queue.put(item)
        finally:
            work_queue.task_done()


def _run_test():

    monitor.start("Insert geo records")

    for num_recs, rec in enumerate(util.retrieve_geo_records()):
        work_queue.put(rec)

    print(
        "Enqueued %d geo records, waiting for "
        "all geo records to be processed" % num_recs)

    work_queue.join()

    monitor.start("Insert geo-correlated file records")

    for num_rec, rec in enumerate(util.retrieve_file_records()):
        work_queue.put(rec)

    print(
        "Enqueued %d data records, waiting for "
        "all data records to be processed" % num_rec)
    work_queue.join()

    print("Queue complete")
    monitor.stop()


def _setup_globals(work_queue_, monitor_):
    global monitor, work_queue
    monitor = monitor_
    work_queue = work_queue_


def run_test(options):
    monitor = avg_rec_rate()
    work_queue = queue.Queue()

    _setup_globals(work_queue, monitor)

    for i in range(options.poolsize):
        thread = threading.Thread(
            target=worker, args=(options, ))
        thread.daemon = True
        thread.start()

    _run_test()

