from ..profile import mproc_avg_rec_rate
from .row_by_row_threaded import worker, _run_test, setup, _setup_globals
import multiprocessing


def run_test(options):
    work_queue = multiprocessing.JoinableQueue()
    monitor = mproc_avg_rec_rate()

    _setup_globals(work_queue, monitor)

    for i in range(options.poolsize):
        thread = multiprocessing.Process(target=worker, args=(options, ))
        thread.daemon = True
        thread.start()

    _run_test()

